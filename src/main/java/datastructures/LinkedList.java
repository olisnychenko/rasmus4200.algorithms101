package datastructures;

public class LinkedList {

    public class Node {
        int data;
        Node next;

        public Node(int data) {
            this.data = data;
        }
    }

    public Node head;
    private int size;

    public int getSize(){
        return size;
    }

    //O(1)
    public void addFront(int data) {
        size++;
        // Create new node
        Node newNode = new Node(data);

        // if head...
        if (head == null) {
            head = newNode;
            return;
        }

        // Set it's next to current head
        newNode.next = head;

        // Set current head equal to this new head
        head = newNode;
    }

    //O(1)
    public int getFirst() {
        if (head == null) {
            throw new IllegalStateException("Empty list!");
        }
        return head.data;
    }

    //O(n)
    public int getLast() {
        if (head == null) {
            throw new IllegalStateException("Empty list!");
        }

        Node current = head;

        // while we are not at the tail
        while (current.next != null) {
            current = current.next; // O(n)
        }

        // We are at the tail
        return current.data;
    }

    //O(n)
    public void addBack(int data) {
        size++;
        Node newNode = new Node(data);
        // if head... set and return
        if (head == null) {
            head = newNode;
            return;
        }

        // Else starting at head...
        Node current = head;

        // Walk until to hit tail
        while (current.next != null) {
            current = current.next;
        }

        // Set current node to equal newNode
        current.next = newNode;
    }

    //O(n)
    public int calcSize() {
        if (head == null) {
            return 0;
        }

        int count = 1;
        Node current = head;

        while (current.next != null) {
            current = current.next;
            count++;
        }
        return count;
    }

    public void clear() {
        size = 0;
        head = null;
    }

    //O(n)
    public void deleteValue(int data) {
        if (size > 0) {
            size--;
        }

        // if head
        if (head == null) {
            return;
         }
        if (head.data == data) {
            head = head.next;
            return;
        }

        // else walk the list
        Node current = head;

        while (current.next != null) {
            //if found
            if (current.next.data == data) {
                //move pointer
                current.next = current.next.next;
                return;
            }
        //move to another node, keep looking
            current = current.next;
        }
    }

    public void print() {
        Node current = head;
        while (current != null) {
            System.out.println(current.data);
            current = current.next;
        }
        System.out.println("");
    }

}
